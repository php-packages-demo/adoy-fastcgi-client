# [adoy/fastcgi-client](https://libraries.io/packagist/adoy%2Ffastcgi-client)

[**adoy/fastcgi-client**](https://packagist.org/packages/adoy/fastcgi-client) [4474](https://phppackages.org/s/fastcgi) Client for communication with a FastCGI (FCGI) application using the FastCGI protocol

# Ported and modernized to latest PHP versions: [hollodotme/fast-cgi-client](https://github.com/hollodotme/fast-cgi-client)
